import os
import json
import time
import logging
import datetime
from logging import config
import yaml
import requests
import connexion
from connexion import NoContent
from pykafka import KafkaClient


YAML_FILE = "openapi.yaml"

if "TARGET_ENV" in os.environ and os.environ["TARGET_ENV"] == "test":
    print("In Test Environment")
    app_conf_file = "/config/app_conf.yml"
    log_conf_file = "/config/log_conf.yml"
else:
    print("In Dev Environment")
    app_conf_file = "app_conf.yml"
    log_conf_file = "log_conf.yml"

with open(app_conf_file, 'r') as f:
    app_config = yaml.safe_load(f.read())

# External Logging Configuration
with open(log_conf_file, 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

logger = logging.getLogger('basicLogger')

logger.info("App Conf File: %s" % app_conf_file)
logger.info("Log Conf File: %s" % log_conf_file)

logger.info("The app is working!")
# Trying to connect to Kafka
hostname = f'{app_config["events"]["hostname"]}:{app_config["events"]["port"]}'
    
max_connection_retry = app_config["events"]["max_retries"]
CURRENT_RETRY_COUNT = 0
#current_retry_count = 0

while CURRENT_RETRY_COUNT < max_connection_retry:
    try:
        logger.info("[Retry # %d] Connecting to Kafka..." % CURRENT_RETRY_COUNT)
        
        client = KafkaClient(hosts=hostname)
        topic = client.topics[str.encode(app_config["events"]["topic"])]
        break
        
    except:
        logger.error("Connection to Kafka failed in retry # %d!" % CURRENT_RETRY_COUNT)
        time.sleep(app_config["events"]["sleep"])
        CURRENT_RETRY_COUNT += 1


def find_restaurant(body) -> NoContent:
    """ Receives a request to find a restaurant """
    # headers = {"Content_Type": "application/json"}
    # response = requests.post(app_config["FindRestaurant"]["url"], json=body, headers=headers)

    logger.info(f'Received event "Find Restaurant" request with a unique id of {body["Restaurant_id"]}')

#     hostname = f'{app_config["events"]["hostname"]}:{app_config["events"]["port"]}'
#     client = KafkaClient(hosts=hostname)
#     topic = client.topics[str.encode(app_config["events"]["topic"])]

    producer = topic.get_sync_producer()

    msg = {"type": "fr",
           "datetime": datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S"),
           "payload": body}

    msg_str = json.dumps(msg)
    producer.produce(msg_str.encode('utf-8'))

    logger.info(f'Returned event "Find Restaurant" response (id: {body["Restaurant_id"]}) with status code 201.')

    return NoContent, 201


def write_review(body) -> NoContent:
    """ Receives a review event """
    # headers = {"Content_Type": "application/json"}
    # response = requests.post(app_config["WriteReview"]["url"], json=body, headers=headers)

    logger.info(f'Received event "Write Review" request with a unique id of {body["Post_id"]}')
    producer = topic.get_sync_producer()
    
    msg = {"type": "wr", 
           "datetime": datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S"), 
           "payload": body}
    
    msg_str = json.dumps(msg)
    producer.produce(msg_str.encode('utf-8'))

    logger.info(f'Returned event "Write Review" response (id: {body["Post_id"]}) with status code 201.')

    return NoContent, 201


app = connexion.FlaskApp(__name__, specification_dir='')
app.add_api(YAML_FILE, strict_validation=True, validate_responses=True)

if __name__ == "__main__":
    app.run(port=8080)
